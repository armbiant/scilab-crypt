//***********************************************************************
// Vincent COUVERT
// Florent LEBEAU
// DIGITEO 2009
// SciCrypt Toolbox
// This file is released under the terms of the CeCILL license.
//***********************************************************************

#include "cphr.hpp"


/*
 * name:	cphr, class constructor
 * @param	symmetric cipher CPHR, cipher mode MODE
 * @return
 */

cphr::cphr(CIPH c, MODE m)
{
	algo = c;
	if(c == AES128)
	{
		nameAlgo = "AES";
		keylength = AES::DEFAULT_KEYLENGTH;
		blocksize = AES::BLOCKSIZE;
	}
	if(c == TDES)
	{
		nameAlgo = "TDES";
		keylength = DES_EDE3::DEFAULT_KEYLENGTH;
		blocksize = DES_EDE3::BLOCKSIZE;
	}
	if(c == SERPENT)
	{
		nameAlgo = "Serpent";
		keylength = Serpent::DEFAULT_KEYLENGTH;
		blocksize = Serpent::BLOCKSIZE;
	}
	if(c == TWFSH)
	{
		nameAlgo = "Twofish";
		keylength = Twofish::DEFAULT_KEYLENGTH;
		blocksize = Twofish::BLOCKSIZE;
	}
	if(c == CST)
	{
		nameAlgo = "CAST256";
		keylength = CAST256::DEFAULT_KEYLENGTH;
		blocksize = CAST256::BLOCKSIZE;
	}
	if(c == BLWFSH)
	{
		nameAlgo = "Blowfish";
		keylength = Blowfish::DEFAULT_KEYLENGTH;
		blocksize = Blowfish::BLOCKSIZE;
	}
	mode = m;
	if(m == CBC)
	{
		nameMode = "CBC";
	}
	if(m == OFB)
	{
		nameMode = "OFB";
	}
	if(m == CFB)
	{
		nameMode = "CFB";
	}
	if(m == ECB)
	{
		nameMode = "ECB";
	}
	if(m == CTR)
	{
		nameMode = "CTR";
	}

	key = NULL;
	iv = NULL;
	outFilename = "default";
}

/*
 *
 * name:	setIV, sets IV
 * @param	hexadecimal string representing the IV to set
 * @return	0 for success, 1 for failure
 */

bool cphr::setIV(string myIV)
{
	delete iv;
	iv = new byte [ blocksize ];
	string sIV;
	StringSource(myIV, true,
		new HexDecoder(
			new StringSink(sIV)
		) // HexDecoder
	); // StringSource
	for(int i = 0; i < blocksize ; i++)
	{
		iv[i] = sIV[i];
	}
	return EXIT_SUCCESS;
}

/*
 *
 * name:	setKey, sets key
 * @param	hexadecimal string representing the key to set
 * @return	0 for success, 1 for failure
 */

bool cphr::setKey(string myKey)
{
	delete key;
	key = new byte [ keylength ];
	string sKey;
	StringSource(myKey, true,
		new HexDecoder(
			new StringSink(sKey)
		) // HexDecoder
	); // StringSource
	for(int i = 0; i < keylength ; i++)
	{
		key[i] = sKey[i];
	}
	return EXIT_SUCCESS;
}

/*
 *
 * name:	setKeyIV, sets key and IV
 * @param	IV and the key to set in hexadecimal
 * @return	0 for success, 1 for failure
 */

bool cphr::setKeyIV(string iv, string key)
{
	bool err1 = setIV(iv);
	bool err2 = setKey(key);
	if(err1)
	{
		cout << "ERROR setting IV" << endl;
		return EXIT_FAILURE;
	}
	if(err2)
	{
		cout << "ERROR setting key" << endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/*
 *
 * name:	setOutFn, changes the name of the output file
 * @param	name
 * @return	0 for success, 1 for failure
 */

bool cphr::setOutFn(string name)
{
    outFilename = name;
}

/*
 *
 * name:	genIV, random IV generation
 * @param
 * @return	0 for success, 1 for failure
 */

bool cphr::genIV()
{
	AutoSeededRandomPool prng;
	delete iv;
	iv = new byte [ blocksize ];
	prng.GenerateBlock( iv, blocksize );
	return EXIT_SUCCESS;
}

/*
 *
 * name:	genKey, random key generation
 * @param
 * @return	0 for success, 1 for failure
 */

bool cphr::genKey()
{
	AutoSeededRandomPool prng;
	delete key;
	key = new byte [ keylength ];
	prng.GenerateBlock( key, keylength );
	return EXIT_SUCCESS;
}

/*
 *
 * name:	genKeyIV, random key and IV generation
 * @param
 * @return	0 for success, 1 for failure
 */

bool cphr::genKeyIV()
{
	int err1 = genIV();
	int err2 = genKey();
	if(err1)
	{
		cout << "ERROR generating IV" << endl;
		return EXIT_FAILURE;
	}
	if(err2)
	{
		cout << "ERROR generating key" << endl;
		if(err1 || err2)
		{
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}

/*
 *
 * name:	getIV, enables IV reading
 * @param
 * @return	string corresponding to the IV in hexadecimal
 */

string cphr::getIV() const
{
	string tmp;
	StringSource( iv, blocksize, true,
		new HexEncoder(
			new StringSink( tmp )
		) // HexEncoder
	); // StringSource
	return tmp;
}

/*
 *
 * name:	getKey, enables key reading
 * @param
 * @return	string corresponding to the key in hexadecimal
 */

string cphr::getKey() const
{
	string tmp;
	StringSource( key, keylength, true,
		new HexEncoder(
			new StringSink( tmp )
		) // HexEncoder
	); // StringSource
	return tmp;
}

/*
 *
 * name:	getOutFn, return output filename
 * @param
 * @return	string corresponding to the name of the output
 */

string cphr::getOutFn() const
{
    return outFilename;
}

/*
 *
 * name:	dspIV, displays IV in hexadecimal
 * @param
 * @return	0 for success, 1 for failure
 */

bool cphr::dspIV() const
{
	if(iv == NULL)
	{
		cout << "IV is not defined" << endl;
		return EXIT_FAILURE;
	}
	string tmp;
	tmp = getIV();
	cout << " IV: " << tmp << endl;
	return EXIT_SUCCESS;
}

/*
 *
 * name:	dspKey, displays key in hexadecimal
 * @param
 * @return	0 for success, 1 for failure
 */

bool cphr::dspKey() const
{
	if(key == NULL)
	{
		cout << "Key is not defined" << endl;
		return EXIT_FAILURE;
	}
	string tmp;
	tmp = getKey();
	cout << "Key: " << tmp << endl;
	return EXIT_SUCCESS;
}

/*
 *
 * name:	dspAll, displays all cipher parameters
 * @param
 * @return	0 for success, 1 for failure
 */

bool cphr::dspAll() const
{
	cout << "Cipher:\t" << nameAlgo << endl;
	cout << "Mode:\t" << nameMode << endl;
	cout << "Blocksize:\t" << blocksize << endl;
	cout << "Keylength:\t" << keylength << endl;
	int n = dspIV() + dspKey();
	if (n > 0 )
	{
		cout << "Missing parameters" << endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/*
 *
 * name:	saveIV, saves the IV (in hexadecimal) into a file
 * @param	outputname
 * @return	0 for success, 1 for failure
 */

bool cphr::saveIV(const string& outputname) const
{
	if(iv == NULL)
	{
		cout << "IV is not defined" << endl;
		return EXIT_FAILURE;
	}
	string tmp;
	StringSource( iv, blocksize, true,
		new HexEncoder(
			new StringSink( tmp )
		) // HexEncoder
	); // StringSource
	FILE* output = fopen(outputname.c_str(), "w");
	if (output == NULL)
	{
		cout << "ERROR creating IV file" << endl;
		return EXIT_FAILURE;
	}
	else
	{
		fputs ((char*)tmp.data(), output);
		fclose (output);
	}

	return EXIT_SUCCESS;
}

/*
 *
 * name:	saveKey, saves the key (in hexadecimal) into a file
 * @param	outputname
 * @return	0 for success, 1 for failure
 */

bool cphr::saveKey(const string& outputname) const
{
	if(key == NULL)
	{
		cout << "Key is not defined" << endl;
		return EXIT_FAILURE;
	}
	string tmp;
	StringSource( key, keylength, true,
		new HexEncoder(
			new StringSink( tmp )
		) // HexEncoder
	); // StringSource
	FILE* output = fopen(outputname.c_str(), "w");
	if (output == NULL)
	{
		cout << "ERROR creating key file" << endl;
		return EXIT_FAILURE;
	}
	else
	{
		fputs ((char*)tmp.data(), output);
		fclose (output);
	}

	return EXIT_SUCCESS;
}

/*
 *
 * name:	saveKeyIV, saves the key and the IV (in hexadecimal) into 2 files
 * @param	keyF
 * 			ivF
 * @return	0 for success, 1 for failure
 */

bool cphr::saveKeyIV(const string& keyF, const string& ivF) const
{
	int err1 = saveIV(ivF);
	int err2 = saveKey(keyF);
	if(err1)
	{
		cout << "ERROR saving IV" << endl;
		return EXIT_FAILURE;
	}
	if(err2 )
	{
		cout << "ERROR saving key" << endl;
		if(err1 || err2)
		{
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}

/*
 *
 * name:	loadIV, loads the IV (in hexadecimal) from a file
 * @param	ivFile
 * @return	0 for success, 1 for failure
 */

bool cphr::loadIV(const string& ivFile)
{
	string siv;
	delete iv;
	iv = new byte [ blocksize ];
	FileSource(ivFile.c_str(), true,
		new HexDecoder(
			new StringSink(siv)
		) // HexDecoder
	); // FileSource
	for(int i = 0; i < blocksize ; i++)
	{
		iv[i] = siv[i];
	}
	return EXIT_SUCCESS;
}

/*
 *
 * name:	loadKey, loads the key (in hexadecimal) from a file
 * @param	keyFile
 * @return	0 for success, 1 for failure
 */

bool cphr::loadKey(const string& keyFile)
{
	string skey;
	delete key;
	key = new byte [ keylength ];
	FileSource(keyFile.c_str(), true,
		new HexDecoder(
			new StringSink(skey)
		) // HexDecoder
	); // FileSource
	for(int i = 0; i < keylength ; i++)
	{
		key[i] = skey[i];
	}
	return EXIT_SUCCESS;
}

/*
 *
 * name:	loadKeyIV, loads the key and the IV (in hexadecimal) from 2 files
 * @param	keyFile
 * 			ivFile
 * @return	0 for success, 1 for failure
 */

bool cphr::loadKeyIV(const string& keyFile, const string& ivFile)
{
	int err1 = loadIV(ivFile);
	int err2 = loadKey(keyFile);
	if(err1)
	{
		cout << "ERROR loading IV" << endl;
		return EXIT_FAILURE;
	}
	if(err2)
	{
		cout << "ERROR loading key" << endl;
		if(err1 || err2)
		{
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}

/*
 *
 * name:	readF, reads a file
 * @param	iFilename, input filename
 * @return	string corresponding to the content
 */

string cphr::readF(const string& iFilename) const
{
	string plain;
	FileSource(iFilename.c_str(), true, new StringSink(plain));
	return plain;
}

/*
 *
 * name:	readCphrF, reads an encrypted file
 * @param	iFilename
 * @return	string corresponding to the hexadecimal representation of
 * 			the encrypted content
 */

string cphr::readCphrF(const string& cipherFile) const
{
	string cipher;
	FileSource(cipherFile.c_str(), true,
		new HexDecoder(
			new StringSink(cipher)
		) // HexDecoder
	); // Filesource
	return cipher;
}

/*
 *
 * name:	createF, creates a file
 * @param	content corresponds to the content of the file to create
 * 			outputname
 * @return	0 for success, 1 for failure
 */

bool cphr::createF(string content, const string& outputname) const
{
	FILE* output = fopen(outputname.c_str(), "w");
	if (output == NULL)
	{
		perror ("Error creating file");
		return EXIT_FAILURE;
	}
	else
	{
		fputs ((char*)content.data(), output);
		fclose (output);
	}

	return EXIT_SUCCESS;
}

/*
 *
 * name:	createCphrF, creates an encrypted file (hexadecimal)
 * @param	cipher, encrypted content of the file to create
 * 			outputname
 * @return	0 for success, 1 for failed
 */

bool cphr::createCphrF(string cipher, const string& outputname) const
{
	string encoded;

	////////////////////////////////////////////////
	// Pretty print (conversion of cipher into hex)
	StringSource( cipher, true,
		new HexEncoder(
			new StringSink( encoded )
		) // HexEncoder
	); // StringSource

	////////////////////////////////////////////////
	//Creating cipher file
	if(createF(encoded, outputname))
	{
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

/*
 * name:	~cphr, class destructor
 * @param
 * @return
 */

cphr::~cphr()
{
	delete key;
	delete iv;
}

/*
 * name:	chkFiles, checks if 2 files are identical or not
 * @param	file1
 * 			file2
 * @return	0 for success, 1 for failure
 */

int chkFiles(char* file1, char* file2)
{
	string content1;
	FileSource i1File(file1, true, new StringSink(content1));
	string content2;
	FileSource i2File(file2, true, new StringSink(content2));
	if(content1 == content2)
	{
		return EXIT_SUCCESS;
	}
	else
	{
		return EXIT_FAILURE;
	}
}
