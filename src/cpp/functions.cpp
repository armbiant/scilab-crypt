//***********************************************************************
// Vincent COUVERT
// Florent LEBEAU
// DIGITEO 2009
// SciCrypt Toolbox
// This file is released under the terms of the CeCILL license.
//***********************************************************************

#include "functions.hpp"

/************************** AES **************************/

int encryptFile_aes(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            aes_ecb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key,plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            aes_cbc algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            aes_ofb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            aes_cfb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            aes_ctr algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int decryptFile_aes(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            aes_ecb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            aes_cbc algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            aes_ofb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            aes_cfb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            aes_ctr algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int keyGen_aes(char* key, char* iv, char* pKey, char* sKey)
{
	aes_cbc algo;
	if(algo.genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(algo.saveKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}
	if(algo.rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	if(algo.save_pubKey(pKey) || algo.save_privKey(sKey))
	{
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/************************** TDES **************************/

int encryptFile_tdes(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            tdes_ecb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key,plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            tdes_cbc algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            tdes_ofb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            tdes_cfb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            tdes_ctr algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int decryptFile_tdes(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            tdes_ecb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            tdes_cbc algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            tdes_ofb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            tdes_cfb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            tdes_ctr algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int keyGen_tdes(char* key, char* iv, char* pKey, char* sKey)
{
	tdes_cbc algo;
	if(algo.genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(algo.saveKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}
	if(algo.rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	if(algo.save_pubKey(pKey) || algo.save_privKey(sKey))
	{
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/************************** Serpent **************************/

int encryptFile_serpent(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            serpent_ecb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key,plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            serpent_cbc algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            serpent_ofb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            serpent_cfb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            serpent_ctr algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int decryptFile_serpent(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            serpent_ecb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            serpent_cbc algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            serpent_ofb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            serpent_cfb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            serpent_ctr algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int keyGen_serpent(char* key, char* iv, char* pKey, char* sKey)
{
	serpent_cbc algo;
	if(algo.genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(algo.saveKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}
	if(algo.rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	if(algo.save_pubKey(pKey) || algo.save_privKey(sKey))
	{
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/************************** TWOFISH **************************/

int encryptFile_twofish(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            twofish_ecb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key,plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            twofish_cbc algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            twofish_ofb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            twofish_cfb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            twofish_ctr algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int decryptFile_twofish(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            twofish_ecb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            twofish_cbc algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            twofish_ofb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            twofish_cfb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            twofish_ctr algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int keyGen_twofish(char* key, char* iv, char* pKey, char* sKey)
{
	twofish_cbc algo;
	if(algo.genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(algo.saveKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}
	if(algo.rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	if(algo.save_pubKey(pKey) || algo.save_privKey(sKey))
	{
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/************************** CAST256 **************************/

int encryptFile_cast256(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            cast256_ecb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key,plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            cast256_cbc algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            cast256_ofb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            cast256_cfb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            cast256_ctr algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int decryptFile_cast256(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            cast256_ecb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            cast256_cbc algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            cast256_ofb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            cast256_cfb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            cast256_ctr algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int keyGen_cast256(char* key, char* iv, char* pKey, char* sKey)
{
	cast256_cbc algo;
	if(algo.genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(algo.saveKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}
	if(algo.rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	if(algo.save_pubKey(pKey) || algo.save_privKey(sKey))
	{
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/************************** BLOWFISH **************************/

int encryptFile_blowfish(char* plainF, char* pKey, char* key, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            blowfish_ecb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key,plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            blowfish_cbc algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            blowfish_ofb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            blowfish_cfb algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            blowfish_ctr algo;
            algo.setOutFn(outFn);
            if(algo.encryption(pKey, key, iv, plainF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int decryptFile_blowfish(char* cipherF, char* sKey, char* cipherKeyF, int m, char* iv, char* outFn)
{
    switch(m)
    {
        case 0: // ECB case: no initialization vector
        {
            blowfish_ecb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        case 1: // CBC
        {
            blowfish_cbc algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 2: // OFB
        {
            blowfish_ofb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 3: // CFB
        {
            blowfish_cfb algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        case 4: // CTR
        {
            blowfish_ctr algo;
            algo.setOutFn(outFn);
            if(algo.decryption(sKey, cipherKeyF, iv, cipherF))
            {
                return EXIT_FAILURE;
            }
            return EXIT_SUCCESS;
        }
        break;
        default: // Error in mode selector
        {
            return EXIT_FAILURE;
        }
        break;
    }
}

int keyGen_blowfish(char* key, char* iv, char* pKey, char* sKey)
{
	blowfish_cbc algo;
	if(algo.genKeyIV())
	{
		return EXIT_FAILURE;
	}
	if(algo.saveKeyIV(key, iv))
	{
		return EXIT_FAILURE;
	}
	if(algo.rsaGenKeys())
	{
		return EXIT_FAILURE;
	}
	if(algo.save_pubKey(pKey) || algo.save_privKey(sKey))
	{
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
