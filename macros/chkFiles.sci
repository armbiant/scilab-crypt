//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

function [ret] = chkFiles (file1, file2)
  cont1 = readc_(file1);
  cont2 = readc_(file2);
  ret = (cont1 == cont2);
endfunction 
