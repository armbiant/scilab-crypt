// ====================================================================
// Allan CORNET
// DIGITEO 2009
// This file is released into the public domain
// ====================================================================

gwy_dir = get_absolute_file_path('cleaner_gateway.sce');

langs = ['c','cpp'];
for l = langs
  if fileinfo(gwy_dir +'/' + l + '/cleaner.sce') <> [] then
    exec(gwy_dir +'/' + l + '/cleaner.sce');
    mdelete(gwy_dir +'/' + l + '/cleaner.sce');
  end
end

clear gwy_dir;
