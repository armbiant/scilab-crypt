/****************** SCICRYPT ******************/
/**************** Version 1.1 *****************/

DESCRIPTION:
SciCrypt is a cyptography toolbox for Scilab. It enables encryption and decryption of files with these block ciphers:
	- AES (128 bits key)
	- TDES (EDE3, 3 * 64 bits keys)
	- Serpent (128 bits key)
	- TwoFish (128 bits key)
	- CAST256 (128 bits key)
	- Blowfish (128 bits key)
Several modes are proposed : 
	- CBC 
	- OFB
	- CFB
	- ECB
	- CTR
Please note that the key used for block ciphering is automatically encoded using the RSA cipher.
Help files, test scripts, and random key generators are included in the toolbox.

INSTALLATION:
Works under linux and Scilab 5.1.
Please execute the following commands with Scilab in the SciCrypt directory:
-->exec builder.sce
-->exec loader.sce

LICENCE:
This toolbox is released under the terms of the CeCILL license.

AUTHOR:
Florent LEBEAU
