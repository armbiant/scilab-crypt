mode(-1)
cd ../samples
keyGen_cast256("sampleKey", "sampleIV", "samplePublicKey", "samplePrivateKey")
// ECB mode
encryptFile_cast256("sampleMatrix.sci", "samplePublicKey", "sampleKey", "0", "sampleIV", "cipher")
decryptFile_cast256("cipher", "samplePrivateKey", "cipherKey", "0", "sampleIV", "recovered")
out = 0;
if chkFiles("sampleMatrix.sci", "recovered")
	disp("cast256 ECB: Success");
	out = out+1;
else
	disp("cast256 ECB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CBC mode
encryptFile_cast256("sampleMatrix.sci", "samplePublicKey", "sampleKey", "1", "sampleIV", "cipher")
decryptFile_cast256("cipher", "samplePrivateKey", "cipherKey", "1", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("cast256 CBC: Success");
	out = out+1;
else
	disp("cast256 CBC: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// OFB mode
encryptFile_cast256("sampleMatrix.sci", "samplePublicKey", "sampleKey", "2", "sampleIV", "cipher")
decryptFile_cast256("cipher", "samplePrivateKey", "cipherKey", "2", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("cast256 OFB: Success");
	out = out+1;
else
	disp("cast256 OFB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CFB mode
encryptFile_cast256("sampleMatrix.sci", "samplePublicKey", "sampleKey", "3", "sampleIV", "cipher")
decryptFile_cast256("cipher", "samplePrivateKey", "cipherKey", "3", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("cast256 CFB: Success");
	out = out+1;
else
	disp("cast256 CFB: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// CTR mode
encryptFile_cast256("sampleMatrix.sci", "samplePublicKey", "sampleKey", "4", "sampleIV", "cipher")
decryptFile_cast256("cipher", "samplePrivateKey", "cipherKey", "4", "sampleIV", "recovered")
if chkFiles("sampleMatrix.sci", "recovered")
	disp("cast256 CTR: Success");
	out = out+1;
else
	disp("cast256 CTR: FAILURE");
end
if MSDOS then unix('del cipher cipherKey recovered');
else unix('rm -f cipher cipherKey recovered'); end
// Conclusions
if out == 5
	disp("cast256 test: Success");
	cast256 = 1;
else
	disp("cast256 test: FAILURE");
	cast256 = 0;
end
deletefile("cipher");
deletefile("cipherKey");
deletefile("recovered");
cd ../tests
