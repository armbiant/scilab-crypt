//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

#ifndef _BLOWFISH_HPP
#define _BLOWFISH_HPP

/*************************** Include C++ ****************************/
#include <iostream>
using namespace std;

/************************* Include Crypto++ *************************/
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include "cryptopp/files.h"
using CryptoPP::FileSink;
using CryptoPP::FileSource;

#include "cryptopp/cryptlib.h"
using namespace CryptoPP;

#include "cryptopp/blowfish.h"
using CryptoPP::Blowfish;

#include "cryptopp/modes.h"
using CryptoPP::OFB_Mode;
using CryptoPP::CBC_Mode;
using CryptoPP::CFB_Mode;

#include "cryptopp/rsa.h"
using CryptoPP::RSA;

/************************* Include SciCrypt *************************/
#include "rsa.hpp"

/********************************************************************/

class blowfish_cbc : public rsa
{
	public:
		blowfish_cbc();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~blowfish_cbc();
};

class blowfish_ofb : public rsa
{
	public:
		blowfish_ofb();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~blowfish_ofb();
};

class blowfish_cfb : public rsa
{
	public:
		blowfish_cfb();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~blowfish_cfb();
};

class blowfish_ecb : public rsa
{
	public:
		blowfish_ecb();
		string enc(string);
		bool encryption(const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&);
		bool KeysGenerator();
		~blowfish_ecb();
};

class blowfish_ctr : public rsa
{
	public:
		blowfish_ctr();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~blowfish_ctr();
};

#endif
