//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

#ifndef _TDES_HPP
#define _TDES_HPP

/*************************** Include C++ ****************************/
#include <iostream>
using namespace std;

/************************* Include Crypto++ *************************/
#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include "cryptopp/files.h"
using CryptoPP::FileSink;
using CryptoPP::FileSource;

#include "cryptopp/cryptlib.h"
using namespace CryptoPP;

#include "cryptopp/des.h"
using CryptoPP::DES_EDE3;

#include "cryptopp/modes.h"
using CryptoPP::OFB_Mode;
using CryptoPP::CBC_Mode;
using CryptoPP::CFB_Mode;

#include "cryptopp/rsa.h"
using CryptoPP::RSA;

/************************* Include SciCrypt *************************/
#include "rsa.hpp"

/********************************************************************/

class tdes_cbc : public rsa
{
	public:
		tdes_cbc();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~tdes_cbc();
};

class tdes_ofb : public rsa
{
	public:
		tdes_ofb();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~tdes_ofb();
};

class tdes_cfb : public rsa
{
	public:
		tdes_cfb();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~tdes_cfb();
};

class tdes_ecb : public rsa
{
	public:
		tdes_ecb();
		string enc(string);
		bool encryption(const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&);
		bool KeysGenerator();
		~tdes_ecb();
};

class tdes_ctr : public rsa
{
	public:
		tdes_ctr();
		string enc(string);
		bool encryption(const string&, const string&, const string&, const string&);
		string dec(string);
		bool decryption(const string&, const string&, const string&, const string&);
		bool KeysGenerator();
		~tdes_ctr();
};

#endif
