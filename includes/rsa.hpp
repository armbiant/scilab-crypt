//***********************************************************************
// Vincent COUVERT							
// Florent LEBEAU							
// DIGITEO 2009								
// SciCrypt Toolbox							
// This file is released under the terms of the CeCILL license.		
//***********************************************************************

#ifndef _RSA_HPP
#define _RSA_HPP

/*************************** Include C++ ****************************/
#include <iostream>
using namespace std;

/************************* Include Crypto++ *************************/
#include "cryptopp/rsa.h"
using CryptoPP::RSA;

#include "cryptopp/files.h"
using CryptoPP::FileSink;
using CryptoPP::FileSource;

#include "cryptopp/hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "cryptopp/osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include "cryptopp/cryptlib.h"
using namespace CryptoPP;

/************************* Include SciCrypt *************************/
#include "cphr.hpp"

/********************************************************************/

class rsa : public cphr
{
	protected:
		InvertibleRSAFunction parameters;	// full rsa parameters
		RSA::PrivateKey privateKey;			// rsa private key (used for decryption)
        RSA::PublicKey publicKey;			// rsa public key (used for decryption)
	public:
		rsa(CIPH, MODE);
		bool rsaGenKeys();
		bool save_pubKey(const string&) const;
		bool save_privKey(const string&) const;
		bool load_pubKey(const string&);
		bool load_privKey(const string&);
		string rsa_enc(string) const;
		string rsa_dec(string) const;
		~rsa();
};
#endif 

